import express from 'express'
import sqlite3 from "sqlite3";
import cors from "cors";
import { open } from "sqlite";

const app = express()

app.use(cors());
app.use(express.json())

app.get('/', function (req, res) {
    res.send('Hello World')
})

app.get("/pokaz-zadania", async (req, res) => {


    const db = await open({
        filename: "./baza.db",
        driver: sqlite3.Database
    });

    const result = await db.all("SELECT * FROM zadania");


    res.json(result)
    // res.send("Tieton, zwany również Dominikiem")
})

app.post("/dodaj-zadanie", async (req, res) => {

    const db = await open({
        filename: "./baza.db",
        driver: sqlite3.Database
    });

    await db.run("INSERT INTO zadania (nazwa, data_dodania, czy_zakonczone) VALUES (?, ?, ?)", req.body.nazwa, Date.now(), 0);

    res.json({ success: true })
})

app.post("/oznacz-jako-wykonane", async (req, res) => {
    res.json({})
})

app.listen(3000, () => console.log("Chuju, serwer działa"))