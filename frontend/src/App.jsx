import axios from "axios";
import { useEffect, useState, useRef } from "react";

const App = () => {
  const [zadania, setZadania] = useState(null);
  const ref = useRef();

  const pobierzDane = async () => {
    const dane = await axios.get("http://localhost:3000/pokaz-zadania");

    setZadania(dane.data);
  };

  useEffect(() => {
    pobierzDane();
  }, []);

  const dodajZadanie = async () => {
    const nazwaZadania = ref.current.value;

    if (!nazwaZadania || nazwaZadania.length < 3) {
      return;
    }

    ref.current.value = "";
    await axios.post("http://localhost:3000/dodaj-zadanie", {
      nazwa: nazwaZadania,
    });
    await pobierzDane();
  };

  return (
    <div className="w-full h-full flex justify-center items-center flex-col gap-8">
      <div className="text-[64px]">Lista zadań</div>
      <input
        ref={ref}
        type="text"
        className="border p-3 w-[400px] font-[Arial] text-lg"
      />
      <div
        onClick={dodajZadanie}
        className="select-none bg-green-500 text-white p-3 rounded hover:opacity-80 transition cursor-pointer"
      >
        Dodaj nowe zadanie
      </div>

      <div>
        <table className="border-collapse border border-slate-500">
          <thead>
            <tr className="bg-slate-400/80 text-white">
              <th className="border border-slate-600 p-2">Id</th>
              <th className="border border-slate-600 p-2">Nazwa</th>
              <th className="border border-slate-600 p-2">Data dodania</th>
              <th className="border border-slate-600 p-2">Wykonane</th>
            </tr>
          </thead>
          <tbody>
            {zadania?.map((t) => (
              <tr key={t.id}>
                <td className="border border-slate-700 p-2">{t.id}</td>
                <td className="border border-slate-700 p-2">{t.nazwa}</td>
                <td className="border border-slate-700 p-2">
                  {new Date(parseInt(t.data_dodania)).toLocaleString()}
                </td>
                <td className="border border-slate-700 p-2">
                  {t.czy_zakonczone ? "tak" : "nie"}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default App;
